import com.nvans.tyrannophone.dal.config.JpaConfig;
import com.nvans.tyrannophone.remote.dao.MessageDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MessageJPATest {

    private GenericApplicationContext ctx;
    private MessageDAO messageDAO;

    @Before
    public void setUp() {

        ctx = new AnnotationConfigApplicationContext(JpaConfig.class);
        messageDAO = ctx.getBean(MessageDAO.class);
        assertNotNull(messageDAO);
    }

    @Test
    public void testFindById() {
        String message = messageDAO.getMessage(1L);
        System.out.println(message);
    }

    @After
    public void tearDown() {
        ctx.close();
    }
}
