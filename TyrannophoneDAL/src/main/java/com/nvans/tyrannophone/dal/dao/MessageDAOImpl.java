package com.nvans.tyrannophone.dal.dao;

import com.nvans.tyrannophone.dal.model.Message;
import com.nvans.tyrannophone.remote.dao.MessageDAO;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class MessageDAOImpl implements MessageDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(readOnly = true)
    @Override
    public String getMessage(Long id) {
        return entityManager
                .createNamedQuery(Message.FIND_MESSAGE_BY_ID, String.class)
                .setParameter("id", id)
                .getSingleResult();
    }
}
