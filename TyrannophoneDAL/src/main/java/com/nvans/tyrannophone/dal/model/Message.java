package com.nvans.tyrannophone.dal.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "message")
@NamedQueries({
        @NamedQuery(name = Message.FIND_MESSAGE_BY_ID,
            query = "select m.message from Message m " +
                    "where m.id = :id")
})
public class Message implements Serializable {

    public static final String FIND_MESSAGE_BY_ID = "Message.findById";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "message")
    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
