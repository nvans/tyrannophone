# Tyrannophone (temorarily placed here: https://github.com/nvans/tyrannophone)
The Telecom service provider information system.

### The project contains 4 related modules: 
 - [TyrannophoneInterface] - Interfaces used in project
 - [TyrannophoneDAL]       - Data Access Layer
 - [TyrannophoneService]   - The Business-logic layer
 - [TyrannophoneWeb]       - The Presentation layer
 
### To deploy project on Tomcat:
```sh
$ cd Tyrannophone
$ mvn tomcat7:deploy
```