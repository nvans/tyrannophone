package com.nvans.tyrannophone.remote.service;

public interface MessageService {

    String getMessage(Long id);
}
