package com.nvans.tyrannophone.remote.dao;

public interface MessageDAO {

    String getMessage(Long id);
}
