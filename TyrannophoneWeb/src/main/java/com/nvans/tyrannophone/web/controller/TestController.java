package com.nvans.tyrannophone.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.nvans.tyrannophone.remote.service.MessageService;

@Controller
public class TestController {

    @Autowired
    private MessageService messageService;

    @GetMapping("/test")
    public String testForm(Model model) {

        model.addAttribute("name", messageService.getMessage(1L));

        return "test";
    }

}
