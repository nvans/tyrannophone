package com.nvans.tyrannophone.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nvans.tyrannophone.remote.dao.MessageDAO;
import com.nvans.tyrannophone.remote.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageDAO messageDAO;

    @Override
    public String getMessage(Long id) {
        return messageDAO.getMessage(id);
    }
}
